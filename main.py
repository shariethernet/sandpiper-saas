import io
import sys
import zipfile

import requests
from zipfile import ZipFile
from io import BytesIO
import argparse


# def create_zip_v1():
#     """
#     returns: zip archive
#     """
#     archive = BytesIO()
#
#     with ZipFile(archive, 'w') as zip_archive:
#         # Create three files on zip archive
#         with zip_archive.open('docker/docker-compose.yaml', 'w') as file1:
#             file1.write(b'compose-file-content...')
#
#         with zip_archive.open('app/app-config.json', 'w') as file2:
#             file2.write(b'app-config-content...')
#
#         with zip_archive.open('root-config.json', 'w') as file3:
#             file3.write(b'root-config-content...')
#
#     return archive
#
#
# archive = create_zip_v1()
#
# # Flush archive stream to a file on disk
# with open('config.zip', 'wb') as f:
#     f.write(archive.getbuffer())
#
# archive.close()
#
#
# resp = requests.post(
#     'https://faas.makerchip.com/function/sandpiper-faas/',
#     files=(
#         ('args', (None, 'sdc')),
#         ('context', ('sandpiper.zip', open('sandpiper.zip', 'rb'))),
#     ),
#     stream=True
# )
#
# print(resp.text)

# with open("./out.zip", 'wb') as fd:
#     for chunk in resp.iter_content(chunk_size=128):
#         fd.write(chunk)

# z = zipfile.ZipFile(BytesIO(resp.content))
# foofile = z.open('stdout')
# print(foofile.read().decode('utf-8'))


def main():
    parser = argparse.ArgumentParser(description='Sandpiper SaaS')
    parser.add_argument('--i', type=str)
    parser.add_argument('--o', type=int)
    args = parser.parse_args()

    zip_buffer = io.BytesIO()
    with zipfile.ZipFile(zip_buffer, "a", zipfile.ZIP_DEFLATED, False) as zip_file:
            zip_file.writestr("test.tlv", open(args.i, 'r').read())


    resp = requests.post(
        'https://faas.makerchip.com/function/sandpiper-faas/',
        files=(
            ('args', (None, '--help')),
            ('context', ('context.zip', zip_buffer.getvalue())),
        ),
        stream=True
    )
    zip_buffer.close()

    z = zipfile.ZipFile(BytesIO(resp.content))
    foofile = z.open('stdout')
    print(foofile.read().decode('utf-8'))

    exit_code = z.open('status').read().decode('utf-8')
    sys.exit(int(exit_code))



if __name__ == "__main__":
    main()
